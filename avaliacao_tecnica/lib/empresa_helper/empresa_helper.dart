class Empresa {

  int id;
  double  taxaDebito, taxaCredito, taxaMinimaDebito, taxaMinimaCredito;
  String nome, cnpj, phone, ramo;

  Empresa(this.id, this.nome, this.cnpj, this.phone, this.ramo, this.taxaDebito, this.taxaCredito);

  addTaxasMinimas(){
    if(ramo == 'Tecnologia'){
      taxaMinimaDebito = 1.0;
      taxaMinimaCredito = 1.5;
    }else if(ramo == 'Sapatos'){
      taxaMinimaDebito = 1.5;
      taxaMinimaCredito = 2.0;
    }else if(ramo == 'Comida'){
      taxaMinimaDebito = 2.0;
      taxaMinimaCredito = 3.0;
    }
  }

}

