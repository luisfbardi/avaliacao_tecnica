import 'package:flutter/material.dart';
import 'package:avaliacao_tecnica/empresa_helper/empresa_helper.dart';
import 'mostra_empresas.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  Empresa tech = Empresa(1, 'Vek', '02.345.786/0001-01', '(48)1111-2222', 'Tecnologia',1.5, 2.5);
  Empresa tech2 = Empresa(2, 'HexStore', '01.765.342/0001-01', '(48)2222-2222', 'Tecnologia',1.3, 2.7);
  Empresa shoes = Empresa(3,'Sapatos 1' , '04.648.719/0001-01', '(48)3333-2222', 'Sapatos',2.0, 3.5);
  Empresa shoes2 = Empresa(4,'Sapatos 2' , '22.534.324/0001-01', '(48)3333-4422', 'Sapatos',2.5, 2.8);
  Empresa food = Empresa(5, 'Padaria', '22.222.333/0001-01', '(48)3553-4422', 'Comida',2.8, 3.8);
  Empresa food2 = Empresa(6, 'Restaurante', '22.111.444/0001-01', '(48)3553-1122','Comida',2.7, 3.9);

  List<Empresa> empresasTech = List();
  List<Empresa> empresasShoes = List();
  List<Empresa> empresasFood = List();

  @override
  void initState() {
    super.initState();

    _addTaxasMinimas();
    _addEmpresasLista();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Selecione o Ramo de Atividade'),
        centerTitle: true,
        backgroundColor: Colors.teal,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.only(left: 50.0, top: 50.0),
        child: Column(
          children: <Widget>[
            Text('Selecione seu Ramo de Atividade',
              style: TextStyle(fontSize: 20.0,
                  fontWeight: FontWeight.bold),
            ),
            Padding(padding: EdgeInsets.only(top: 100.0)),
            RaisedButton(
                color: Colors.teal,
                textColor: Colors.white,
                elevation: 5.0,
                child: Text('   Ramo Tech   '),
                onPressed: (){
                  _showEmpresas(empresasTech);
                }
            ),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            RaisedButton(
                color: Colors.teal,
                textColor: Colors.white,
                elevation: 5.0,
                child: Text('Ramo Sapatos'),
                onPressed: (){
                  _showEmpresas(empresasShoes);
                }
            ),
            Padding(padding: EdgeInsets.only(top: 30.0)),
            RaisedButton(
                color: Colors.teal,
                textColor: Colors.white,
                elevation: 5.0,
                child: Text('Ramo Comida'),
                onPressed: (){
                  _showEmpresas(empresasFood);
                }
            ),
          ],
        ),
      ),
    );
  }

  _addTaxasMinimas(){
    tech.addTaxasMinimas();
    tech2.addTaxasMinimas();
    shoes.addTaxasMinimas();
    shoes2.addTaxasMinimas();
    food.addTaxasMinimas();
    food2.addTaxasMinimas();
  }
  _addEmpresasLista(){
    empresasTech.add(tech);
    empresasTech.add(tech2);
    empresasShoes.add(shoes);
    empresasShoes.add(shoes2);
    empresasFood.add(food);
    empresasFood.add(food2);
  }

  void _showEmpresas(List<Empresa> empresa){
    Navigator.push(context,
        MaterialPageRoute(builder: (context)=> MostraEmpresas(empresa))
    );
  }

}
