import 'package:flutter/material.dart';
import 'package:avaliacao_tecnica/empresa_helper/empresa_helper.dart';
import 'descount_page.dart';

class MostraEmpresas extends StatefulWidget {

  final List<Empresa> empresas;

  const MostraEmpresas(this.empresas);

  @override
  _MostraEmpresasState createState() => _MostraEmpresasState();
}

class _MostraEmpresasState extends State<MostraEmpresas> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Concorrentes'),
        backgroundColor: Colors.teal,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            _empresaCard(widget.empresas[0]),
            _empresaCard(widget.empresas[1])
          ],
      ),
    );
  }
  Widget _empresaCard(Empresa empresa){
    return GestureDetector(
      child: Card(
        elevation: 5.0,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              Text(empresa.nome,
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: EdgeInsets.only(left: 30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text('Ramo: ', style: TextStyle(fontSize: 13.0),),
                        Text(empresa.ramo,
                          style: TextStyle(fontSize: 13.0),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('CNPJ: ', style: TextStyle(fontSize: 13.0),),
                        Text(empresa.cnpj,
                          style: TextStyle(fontSize: 13.0),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('Taxa Debito: ', style: TextStyle(fontSize: 13.0),),
                        Text(empresa.taxaDebito.toString(),
                          style: TextStyle(fontSize: 13.0),
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text('Taxa Credito: ', style: TextStyle(fontSize: 13.0),),
                        Text(empresa.taxaCredito.toString(),
                          style: TextStyle(fontSize: 13.0),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      onTap: (){
        _showDescountPage(empresa);
      },
    );
  }
  void _showDescountPage(Empresa empresa){
    Navigator.push(context,
        MaterialPageRoute(builder: (context)=> DescountPage(empresa))
    );
  }
}
