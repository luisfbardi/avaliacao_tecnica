import 'dart:io';
import 'package:avaliacao_tecnica/empresa_helper/empresa_helper.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class DescountPage extends StatefulWidget {

   Empresa empresa;

   DescountPage(this.empresa);

  @override
  _DescountPageState createState() => _DescountPageState();
}

class _DescountPageState extends State<DescountPage> {

  final _descontoDebitoController = TextEditingController();
  final _descontoCreditoController = TextEditingController();

  List<String> historicoTaxasDebito = List();
  List<String> historicoTaxasCredito = List();

  List<List> taxasAprovadasDebito = List();
  List<List> taxasAprovadasCredito = List();
  List<List> taxasReprovadasDebito = List();
  List<List> taxasReprovadasCredito = List();

  double descontoDebito, descontoCredito, novaTaxaDebito, novaTaxaCredito;

  String nomeCsv;
  var now = DateTime.now();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: Text('Descontos'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(top: 100.0)),
                  Text(widget.empresa.nome,
                    style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('Ramo: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.ramo,
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('CNPJ: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.cnpj,
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('Telefone: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.phone,
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('Taxa Debito: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.taxaDebito.toString(),
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('Taxa Credito: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.taxaCredito.toString(),
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('Taxa Minima Debito: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.taxaMinimaDebito.toString(),
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 20.0, top: 30.0)),
                  Text('Taxa Minima Credito: ', style: TextStyle(fontSize: 20.0),),
                  Text(widget.empresa.taxaMinimaCredito.toString(),
                    style: TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(padding: EdgeInsets.only( top: 100.0)),
                  Container(
                      width: 130.0,
                      height: 40.0,
                      child: RaisedButton(
                          padding: EdgeInsets.only(left: 3.0),
                          color: Colors.teal,
                          textColor: Colors.white,
                          elevation: 5.0,
                          child: Text('Desconto Debito'),
                          onPressed: (){
                            _descontoDebitoMessage('Desconto Debito');
                          }
                      )
                  ),
                  Padding(
                      padding: EdgeInsets.only(right: 20.0)
                  ),
                  Container(
                      width: 130.0,
                      height: 40.0,
                      child: RaisedButton(
                          padding: EdgeInsets.only(left: 3.0),
                          color: Colors.teal,
                          textColor: Colors.white,
                          elevation: 5.0,
                          child: Text('Desconto Credito'),
                          onPressed: (){
                            _descontoCreditoMessage('Desconto Credito');
                          }
                      )
                  )
                ],
              ),
            ],
          ),
        )
    );
  }

  Future<void> _errorMessage(String titulo, String texto, String botao) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(titulo),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(texto),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(botao),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _descontoDebitoMessage(String tipoDesconto) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(tipoDesconto),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                    controller: _descontoDebitoController,
                    decoration: InputDecoration(hintText: '1 - 99(%)'),
                    onChanged: (text){
                      if(double.parse(text) > 0 && double.parse(text) < 100){
                        descontoDebito = double.parse(text);
                      }else{
                        _errorMessage('Desconto Debito Invalido!', 'Preencha o Desconto com um valor entre 1 e 99.', 'Okay');
                        _descontoDebitoController.text = '';
                      }
                    },
                    keyboardType: TextInputType.number
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Voltar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Calcular'),
              onPressed: () {
                _calculaDescontoDebito(descontoDebito, widget.empresa.taxaDebito);
                _descontoDebitoController.text ='';
                _mostraNovaTaxaDebito(novaTaxaDebito.toStringAsPrecision(3));
              },
            ),
          ],
        );
      },
    );
  }
  Future<void> _descontoCreditoMessage(String tipoDesconto) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(tipoDesconto),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                TextField(
                    controller: _descontoCreditoController,
                    decoration: InputDecoration(hintText: '1 - 99(%)'),
                    onChanged: (text){
                      if(double.parse(text) > 0 && double.parse(text) < 100){
                        descontoCredito = double.parse(text);
                      }else if (double.parse(text) == null || double.parse(text) == ''){
                        _errorMessage('Desconto Nulo', 'Por favor insira um Desconto Valido', 'Okay');
                      }else{
                        _errorMessage('Desconto Credito Invalido!', 'Preencha o Desconto com um valor entre 1 e 99.', 'Okay');
                        _descontoCreditoController.text = '';
                      }
                    },
                    keyboardType: TextInputType.number
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Voltar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Calcular'),
              onPressed: () {
               _calculaDescontoCredito(descontoCredito, widget.empresa.taxaCredito);
               _descontoCreditoController.text ='';
               _mostraNovaTaxaCredito(novaTaxaCredito.toStringAsPrecision(3));
              },
            ),
          ],
        );
      },
    );
  }

  _calculaDescontoDebito(double desconto, double taxa) {
    novaTaxaDebito = taxa - ((desconto/100) * taxa);
    if(novaTaxaDebito < widget.empresa.taxaMinimaDebito){
      _errorMessage('Desconto Muito Baixo', 'Desconto abaixo da taxa minima\n por favor tente outra %', 'Okay');
    }else if (novaTaxaDebito > widget.empresa.taxaDebito){
      _errorMessage('Taxa Aumentada', 'A nova taxa deve ser menor do que a taxa atual\n por favor tente outra %', 'Okay');
    }else{
      return novaTaxaDebito;
    }
  }
  _calculaDescontoCredito(double desconto, double taxa) {
    novaTaxaCredito = taxa - ((desconto/100) * taxa);
    if(novaTaxaCredito < widget.empresa.taxaMinimaCredito){
      _errorMessage('Desconto Muito Baixo', 'Desconto abaixo da taxa minima\n por favor tente outra %', 'Okay');
    }else if (novaTaxaCredito > widget.empresa.taxaCredito){
      _errorMessage('Taxa Aumentada', 'A nova taxa deve ser menor do que a taxa atual\n por favor tente outra %', 'Okay');
    }else{
      return novaTaxaCredito;
    }
  }

  Future<void> _mostraNovaTaxaDebito(String novaTaxa) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Nova Taxa Debito'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('A nova taxa é: $novaTaxa\n'),
                Text('O cliente aceita a proposta?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceita'),
              onPressed: () {
                _addHistoricotaxasDebito(taxasAprovadasDebito);
                nomeCsv = 'Taxas Aprovadas Debito';
                _messageCSV(taxasAprovadasDebito, 'Aprovadas');
              },
            ),
            FlatButton(
              child: Text('Recusa'),
              onPressed: () {
                _addHistoricotaxasDebito(taxasReprovadasDebito);
                nomeCsv = 'Taxas Reprovadas Debito';
                _messageCSV(taxasReprovadasDebito, 'Reprovadas');
              },
            )
          ],
        );
      },
    );
  }
  Future<void> _mostraNovaTaxaCredito(String novaTaxa) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Nova Taxa Credito'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('A nova taxa é: $novaTaxa'),
                Text('O cliente aceita a proposta?')
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Aceita'),
              onPressed: () {
                _addHistoricotaxasCredito(taxasAprovadasCredito);
                nomeCsv = 'Taxas Aprovadas Credito';
                _messageCSV(taxasAprovadasCredito, 'Aprovadas');
              },
            ),
            FlatButton(
              child: Text('Recusa'),
              onPressed: () {
                _addHistoricotaxasDebito(taxasReprovadasCredito);
                nomeCsv = 'Taxas Reprovadas Credito';
                _messageCSV(taxasReprovadasCredito, 'Reprovadas');
              },
            )
          ],
        );
      },
    );
  }

  Future<File> _getCsvFile() async {
    String directory = (await getExternalStorageDirectory()).absolute.path;
    return File('$directory/$nomeCsv.csv');
  }
  Future<File> saveCSV(List lista) async {
    String csv = lista.toString();
    final file = await _getCsvFile();
    return file.writeAsString(csv);
  }
  Future<void> _messageCSV(List<List> list, String aprovadaOuReprovada) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Salvar CSV'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Deseja salvar um arquivo csv com o historico atual de Taxas $aprovadaOuReprovada?'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Sim'),
              onPressed: () {
                saveCSV(list);
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Nao'),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  _addHistoricotaxasDebito(List<List> list){
    historicoTaxasDebito.add(widget.empresa.nome+';');
    historicoTaxasDebito.add(widget.empresa.cnpj+';');
    historicoTaxasDebito.add(widget.empresa.ramo+';');
    historicoTaxasDebito.add(widget.empresa.phone+';');
    historicoTaxasDebito.add(widget.empresa.taxaDebito.toString()+';');
    historicoTaxasDebito.add(widget.empresa.taxaCredito.toString()+';');
    historicoTaxasDebito.add(descontoDebito.toString()+';');
    historicoTaxasDebito.add(novaTaxaDebito.toString()+';');
    historicoTaxasDebito.add(now.toString());
    list.add(historicoTaxasDebito);
  }
  _addHistoricotaxasCredito(List<List> list){
    historicoTaxasCredito.add(widget.empresa.nome+';');
    historicoTaxasCredito.add(widget.empresa.cnpj+';');
    historicoTaxasCredito.add(widget.empresa.ramo+';');
    historicoTaxasCredito.add(widget.empresa.phone+';');
    historicoTaxasCredito.add(widget.empresa.taxaDebito.toString()+';');
    historicoTaxasCredito.add(widget.empresa.taxaCredito.toString()+';');
    historicoTaxasCredito.add(descontoCredito.toString()+';');
    historicoTaxasCredito.add(novaTaxaCredito.toString()+';');
    historicoTaxasCredito.add(now.toString());
    list.add(historicoTaxasCredito);
  }
}
